﻿#include "pch.h"
#include <iostream>
#include <math.h>
#include <fstream>
#include <cstdlib>


using namespace std;

//CONST
#define K0_const 1E12 //секунда^(-1)
#define Estatic 1 //еВ
#define k_bol 8.625E-5 //константа Больцмана еВ/К
#define Temperature 300 // Kельвин
#define RAST 0.9 // расстояние между клетками решетки

#define nDEBUG

void create_rate_catalog(int * mas, int * _mas_from_to, double * _rate);

int main()
{
	ofstream fout;
	fout.open("file.txt", ios::out);
	fout << "from" << "\t" << "to" << "\t" << " rate" << "\t" << "\t time" << endl;
	srand(5168);
	int mas[9] = {}; //кристалическая решетка 3х3
	double time = 0;
	int *mas_from_to = new int[36];
	double *rate = new double[18];

	//сажаем атомы в решетку
	int type = 1; //коли-во типов атомов
	int num_at = 3; //кол-во атомов
	for (int i = 0; i < num_at; i++) {
		mas[i] = 1; // 1- признак наличия атома в кр. реш-ке
	}

	for (int i = 0; i < 10; i++) 
	{
		create_rate_catalog(mas, mas_from_to, rate);
		//Выбор одного из путей перехода
		double * sum = new double[18];
		sum[0] = rate[0];
		for (int i = 1; i < 18; i++) {
			sum[i] = sum[i - 1] + rate[i];
		}
		double rand1 = ((double)(rand()) / RAND_MAX) * sum[17];

		int j = 0;
		for (j; sum[j] <= rand1; j++) {}
		delete[] sum;
		//увеличить время time
		time += (-1 / rate[j] * log(((double)(rand()) / RAND_MAX)));
#ifndef DEBUG
		//основной вывод
		fout << (mas_from_to[2 * j + 1]) << "\t" << (mas_from_to[2 * j]) << "\t" << rate[j]  << "\t" << time << endl;
		mas[(mas_from_to[2 * j + 1])] = mas[(mas_from_to[2 * j])];
		mas[(mas_from_to[2 * j])] = 0;
#endif
		
	}

#ifdef DEBUG
	//вывод отладки
	cout << rand3 << endl;
	cout << rand2 << endl;
	cout << rand1 << endl;
	cout << j << endl;
	for (int i = 0; i < 18; i++) {
		printf("sum = %f ", sum[i]);
		printf("rate  = %.8e \n", rate[i]);
	}
	cout << endl;

	for (int i = 0; i < 9; i++) {
		cout << mas[i] << ' ';
	}

#endif
	fout.close();
	system("file.txt");
	delete[] rate;
	delete [] mas_from_to;
	return 0;
}

void create_rate_catalog(int * mas, int * _mas_from_to, double * _rate) {
	//составляем таблицу скоростей(RATE CATALOG)
	/*RATE CATALOG состоит из двух массивов:
		mas_from_to:
		четный элемент хранит позицию атома, кот переходит
		нечетный - позицию куда переходит
		rate
		хранит константу скорости для соответствующего перехода
	*/

	for (int i = 0; i < 9; i++) {
		if (mas[i] > 0) { // если в i ячейке есть атом
			for (int k = 0; k < 9; k++) { //начинаем перебирать все возможные его переходы
				if (mas[k] == 0) { // если k ячейка сободна
					*_mas_from_to = i; // 2n ый элемент массива RATE CATALOG это from
					_mas_from_to++;
					*_mas_from_to = k; // 2n+1 ый элемент массива RATE CATALOG это to
					_mas_from_to++;
					// определяем координаты в клетке
					int xf = i % 3;
					int yf = i / 3;
					int xt = k % 3;
					int yt = k / 3;
					// определяем растояние между ячейками from и to
					double r = sqrt(double((xf - xt)*(xf - xt) + (yf - yt)*(yf - yt)));
					switch (mas[i]) { // какой тип атома будет переходить
					case 1:
						//записываем константу скорости в RATE CATALOG
						*_rate = (double)(K0_const)* exp((double)((-1)* Estatic / (k_bol) / Temperature * r * RAST));
						_rate++;
						break;
					case 0:
						break;
					}
				}
			}
		}
	}
}